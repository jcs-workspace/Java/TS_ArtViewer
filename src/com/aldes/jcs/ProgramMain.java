package com.aldes.jcs;

import java.io.IOException;

import com.aldes.jcs.core.Model;
import com.aldes.jcsqtj.core.JCSQtJ_Window;
import com.aldes.jcsqtj.util.JCSQtJ_FilenameUtils;
import com.trolltech.qt.gui.QComboBox;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QLineEdit;
import com.trolltech.qt.gui.QTextEdit;

/**
 * $File: ProgramMain.java $
 * $Date: 2017-04-26 16:04:58 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

public class ProgramMain {
    
    public static JCSQtJ_Window MAIN_WINDOW = null;
    
    public static String WINDOW_TITLE = "Totem Spirits Character Viewer Tool";
    public static String DATA_PATH = "data/";
    public static String ICON_IMAGE_PATH = DATA_PATH + "icon.ico";
    
    public static int WINDOW_WIDTH = 800;
    public static int WINDOW_HEIGHT = 600;
    
    /* Combo Box */
    public static QComboBox SKIN_COLOR_CB = null;
    public static QComboBox TRIBE_CB = null;
    public static QComboBox ACTION_CB = null;
    public static QComboBox ANIM_VER_CB = null;
    public static QComboBox FRAME_CB = null;
    
    /* Check Box */
    
    /* Text Edit */
    public static QTextEdit EXPORT_PATH_TE = null;
    
    /* Line Edit */
    public static QLineEdit EXPORT_FILENAME_LE = null;
    
    /* Label */

    /* Image */
    private static String BG_IMG_PATH = "res/Transparency500";
    public static String BASE_IMG_PATH = DATA_PATH + "Human/Body/";
    
    /* Track down all the image layer file. */
    public static QLabel BASE_IMG_LABEL = null;
    public static QLabel HAIR_LABEL = null;
    public static QLabel EYES_LABEL = null;
    public static QLabel HAT_LABEL = null;
    public static QLabel CLOTH_LABEL = null;
    public static QLabel PANTS_LABEL = null;
    public static QLabel SHOE_LABEL = null;
    public static QLabel GLOVE_LABEL = null;
    public static QLabel EAR_LABEL = null;
    public static QLabel SHEILD_LABEL = null;
    public static QLabel WEAPON_LABEL = null;
    public static QLabel FACE_EXP_LABEL = null;
    
    /* Track down the source file path in order to export. */
    public static String TARGET_BASE_IMG_PATH = "";
    
    /* Export settings. */
    public static String EXPORT_FILENAME = "default_export_file_name";
    public static String EXPORT_PATH = System.getProperty("user.home");      // use this with file chooser.
    public static String EXPORT_FULL_PATH = System.getProperty("user.home") + EXPORT_FILENAME;
    
    
    /**
     * @func main
     * @brief Program entry, testing purpose.
     * @param args : arguments vector.
     * @throws IOException 
     */
    public final static void main(final String[] args) throws IOException {
        
        MAIN_WINDOW = new JCSQtJ_Window();
        MAIN_WINDOW.startFrame(args);
        
        MAIN_WINDOW.setWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        MAIN_WINDOW.setWindowTitle(WINDOW_TITLE);
        MAIN_WINDOW.setIconImage(ICON_IMAGE_PATH);
        
        Model model = new Model();        // methods
        
        /* Button */
        MAIN_WINDOW.setDefualtButtonWidth(100);
        MAIN_WINDOW.setDefualtButtonHeight(30);
        MAIN_WINDOW.addButtonToWindow("Export", 675, 545, model, "exportImage()");
        MAIN_WINDOW.addButtonToWindow("Choose Directory", 570, 545, model, "chooseFile()");
        
        /* Combo Box */
        MAIN_WINDOW.setDefualtComboBoxWidth(80);
        MAIN_WINDOW.setDefualtComboBoxHeight(30);
        
        SKIN_COLOR_CB = MAIN_WINDOW.addComboBoxToWindow(530, 60);
        SKIN_COLOR_CB.addItem("Light Skin");
        SKIN_COLOR_CB.addItem("Tanned Skin");
        SKIN_COLOR_CB.addItem("Dark Skin");
        SKIN_COLOR_CB.addItem("Pale Skin");
        
        TRIBE_CB = MAIN_WINDOW.addComboBoxToWindow(620, 60);
        TRIBE_CB.addItem("Human");
        TRIBE_CB.addItem("Elf");
        TRIBE_CB.addItem("Angel");
        
        ACTION_CB = MAIN_WINDOW.addComboBoxToWindow(710, 60);
        ACTION_CB.addItem("Stand");
        ACTION_CB.addItem("Walk");
        ACTION_CB.addItem("Jump");
        ACTION_CB.addItem("Alert");
        ACTION_CB.addItem("Rope");
        ACTION_CB.addItem("Ladder");
        ACTION_CB.addItem("Prone");
        ACTION_CB.addItem("Sit");
        ACTION_CB.addItem("Hit");
        ACTION_CB.addItem("Die");
        ACTION_CB.addItem("Fly");
        ACTION_CB.addItem("Swim");
        ACTION_CB.addItem("Dance");
        ACTION_CB.addItem("Ghost");
        
        FRAME_CB = MAIN_WINDOW.addComboBoxToWindow(710, 140);
        FRAME_CB.addItem("0");
        
        ANIM_VER_CB = MAIN_WINDOW.addComboBoxToWindow(710, 100);
        ANIM_VER_CB.addItem("1");
        
        /* Text Edit */
        EXPORT_PATH_TE = MAIN_WINDOW.addTextEditToWindow(20, 530, 500, 60);
        EXPORT_PATH_TE.setEnabled(false);
        EXPORT_PATH_TE.setText(
                System.getProperty("user.home") + 
                "\\" + 
                JCSQtJ_FilenameUtils.safeAddExtensionPNG(EXPORT_FILENAME));
        
        /* Line Edit */
        EXPORT_FILENAME_LE = MAIN_WINDOW.addLineEditToWindow(600, 20, 190, 30);
        EXPORT_FILENAME_LE.setText(EXPORT_FILENAME);  // set default file name.
        
        /* Label */
        MAIN_WINDOW.addLabelToWindow("Anim Ver.: ", 640, 100);
        MAIN_WINDOW.addLabelToWindow("Frame: ", 640, 140);
        MAIN_WINDOW.addLabelToWindow("File Name: ", 530, 20);
        
        /* Image */
        // add transparent bg image
        MAIN_WINDOW.addImageToWindow(BG_IMG_PATH, 20, 20, 500, 500);
        
        model.updateBase();
        
        
        // -----------------------------------------------------
        MAIN_WINDOW.endFrame();
    }
    
}
