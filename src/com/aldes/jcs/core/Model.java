package com.aldes.jcs.core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.aldes.jcs.ProgramMain;
import com.aldes.jcs.test.CombineImage;
import com.aldes.jcsqtj.util.JCSQtJ_FilenameUtils;
import com.aldes.jcsqtj.util.JCSQtJ_Util;
import com.trolltech.qt.gui.QFileDialog;

public class Model {
    
    
    
    public Model() {
        
    }
    
    /**
     * Export out the image layer by layer.
     * @throws IOException 
     */
    public void exportImage() throws IOException {
        CombineImage.combineImages(
                ProgramMain.EXPORT_FULL_PATH, 
                /* Layers */
                ProgramMain.TARGET_BASE_IMG_PATH);
    }
    
    /**
     * Update Combo box base on current animation.
     * @throws IOException 
     */
    public void updateFrameForCB() {
        
    }
    
    /**
     * Open the file chooser.
     */
    public void chooseFile() {
        String directoryPath = QFileDialog.getExistingDirectory();
        
        // check if cancel selection.
        if (directoryPath.equals(""))
            return;
        
        // update export path.
        ProgramMain.EXPORT_PATH = directoryPath;
        ProgramMain.EXPORT_PATH = ProgramMain.EXPORT_PATH.replace("\\", "/");
        ProgramMain.EXPORT_PATH += "/";
        
        // get current filename from 'line edit'.
        ProgramMain.EXPORT_FILENAME = ProgramMain.EXPORT_FILENAME_LE.text();
        
        // get full path
        ProgramMain.EXPORT_FULL_PATH = 
                ProgramMain.EXPORT_PATH +
                JCSQtJ_FilenameUtils.safeAddExtensionPNG(ProgramMain.EXPORT_FILENAME);
        
        // update label
        ProgramMain.EXPORT_PATH_TE.setText(ProgramMain.EXPORT_FULL_PATH);
    }
    
    /**
     * If animation, try to get the next frame.
     */
    public void nextFrame() {
        
    }
    
    /**
     * If animation, try to get the previous frame.
     */
    public void previousFrame() {
        
    }
    
    /**
     * Update base animation sprite.
     * @throws IOException 
     */
    public void updateBase() throws IOException {
        String basePath = 
                ProgramMain.BASE_IMG_PATH + 
                ProgramMain.SKIN_COLOR_CB.currentText() + 
                "/" + 
                ProgramMain.ACTION_CB.currentText().toLowerCase() +
                ProgramMain.ANIM_VER_CB.currentText() +
                "_" +
                ProgramMain.FRAME_CB.currentIndex();
        
        basePath = JCSQtJ_FilenameUtils.safeAddExtensionPNG(basePath);
        
        System.out.println();
        
        if (ProgramMain.BASE_IMG_LABEL == null) {
            BufferedImage baseImage = ImageIO.read(new File(basePath));
            ProgramMain.BASE_IMG_LABEL = 
                    ProgramMain.MAIN_WINDOW.addImageToWindow(
                            basePath, 
                            /* set it to center */
                            270 - baseImage.getWidth() / 2, 
                            270 - baseImage.getHeight() / 2);
        }
        else {
            // simply set the image to label.
            JCSQtJ_Util.addImageToLabel(ProgramMain.BASE_IMG_LABEL, basePath);   
        }
    }
    
}
